function deleteCookie(name) {
  document.cookie = name + '=;expires=Thu, 01 Jan 1970 00:00:01 GMT;';
  window.location.href = '/auth/login';
}

document.getElementById('logout').addEventListener('click', function () { deleteCookie('icpc_auth'); });
