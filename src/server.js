require('dotenv').config();
const config = require('./lib/config');
const Fastify = require('fastify');
const fastifyStatic = require('fastify-static');
const fastifyFormBody = require('fastify-formbody');
const fastifyCookie = require('fastify-cookie');
const fastifyHelmet = require('fastify-helmet');
const { join } = require('path');
const db = require('./lib/db');
const logger = require('./logger.server');
const {
  home,
  getLogin,
  postLogin,
  getSignup,
  postSignup,
  dashboard,
  internalErrorHandler,
  paymentHandler,
  paymentRedirectHandler,
  fourOhFourHandler,
  postDashboard,
} = require('./handlers');
const { IdpayPayment } = require('./services/idpay');


db.connect();
const idpay = new IdpayPayment();

const server = Fastify({ logger, trustProxy: config.server.trustProxy });

if (config.logging.simple.logRequestBody) {
  server.addHook('preHandler', (request, reply, done) => {
    if (request.body) {
      request.log.info(
        { body: request.body },
        'parsed request body',
      );
    }

    done();
  });
}

if (config.logging.simple.logResponseBody) {
  server.addHook('onSend', async (request, reply, payload) => {
    if (payload) {
      request.log.info(
        { replyChunk: `${payload}` },
        'reply body chunk',
      );
    }

    return payload;
  });
}

server.decorateReply('html', function html(html) {
  this.type('text/html').send(html);
});

server.register(fastifyCookie);
server.register(fastifyFormBody);
server.register(fastifyStatic, {
  root: join(__dirname, '..', 'public'),
  prefix: '/public/',
});


// Routes
server.route({
  method: 'GET',
  url: '/internal-error',
  handler: (request, reply) => internalErrorHandler(request, reply),
});

server.route({
  method: 'GET',
  url: '/',
  handler: (request, reply) => home(request, reply),
});

server.route({
  method: 'GET',
  url: '/auth/login',
  handler: (request, reply) => getLogin(request, reply),
});

server.route({
  method: 'POST',
  url: '/pay/:teamId',
  handler: (request, reply) => paymentHandler(idpay)(request, reply),
});

server.route({
  method: 'POST',
  url: '/verify-payment',
  handler: (request, reply) => paymentRedirectHandler(idpay)(request, reply),
});

server.route({
  method: 'POST',
  url: '/auth/login',
  handler: (request, reply) => postLogin(request, reply),
});

server.route({
  method: 'GET',
  url: '/auth/signup',
  handler: (request, reply) => getSignup(request, reply),
});

server.route({
  method: 'POST',
  url: '/auth/signup',
  handler: (request, reply) => postSignup(request, reply),
});

server.route({
  method: 'GET',
  url: '/dashboard',
  handler: (request, reply) => dashboard(request, reply),
});

server.route({
  method: 'POST',
  url: '/dashboard',
  handler: (request, reply) => postDashboard(request, reply),
});

server.route({
  method: 'GET',
  url: '/*',
  handler: (request, reply) => fourOhFourHandler(request, reply),
});

server.register(fastifyHelmet, {
  hidePoweredBy: { setTo: 'PHP/7.3.11-1+ubuntu19.10.1+deb.sury.org+6' },
  featurePolicy: {
    features: {
      autoplay: ['\'none\''],
      camera: ['\'none\''],
      geolocation: ['\'none\''],
      gyroscope: ['\'none\''],
      microphone: ['\'none\''],
      midi: ['\'none\''],
      notifications: ['\'none\''],
      speaker: ['\'none\''],
      usb: ['\'none\''],
      vr: ['\'none\''],
      syncXhr: ['\'none\''],
      fullscreen: ['\'none\''],
      vibrate: ['\'none\''],
      payment: ['\'none\''],
    },
  },
  permittedCrossDomainPolicies: {
    permittedPolicies: 'none',
  },
  contentSecurityPolicy: {
    directives: {
      'default-src': ['\'self\''],
      'script-src': ['\'self\'', 'https://cdn.jsdelivr.net/npm/jquery@3.4.1/dist/jquery.min.js', 'https://code.jquery.com/jquery-1.10.2.js'],
      'font-src': ['\'self\'', 'https://fonts.googleapis.com', 'https://fonts.gstatic.com', 'data:'],
      'style-src': ['\'self\'', 'https://fonts.googleapis.com'],
      'img-src': ['\'self\'', 'https://via.placeholder.com', 'data:'],
    },
    browserSniff: false,
  },
  referrerPolicy: {
    policy: 'same-origin',
  },
});

server.addHook('onSend', async (request, reply) => {
  if (!reply.hasHeader('Server')) {
    reply.header('Server', 'AmazonS3');
  }
});

const PORT = config.server.port;
server.listen(PORT, (error) => {
  if (error) {
    server.log.error(error, 'Error while starting server');
    return;
  }
});


async function onExit() {
  await server.close();
  logger.info('Server closed');

  await db.disconnect();
  logger.info('Database disconnected');
  process.exit();
}
// catches ctrl+c event
process.on('SIGINT', onExit);

// catches "kill pid" (for example: nodemon restart)
process.on('SIGUSR1', onExit);
process.on('SIGUSR2', onExit);

//catches uncaught exceptions
process.on('uncaughtException', onExit);
