const { memberModel } = require('./member');
const { teamModel } = require('./team');


module.exports = {
  memberModel,
  teamModel,
};
