const mongoose = require('mongoose');


const TeamSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
    unique: true,
    index: {
      background: false,
      unique: true,
    },
  },
  registeredAt: {
    type: String,
    required: true,
  },
  registrarId: {
    type: String,
    required: true,
    unique: true,
    index: {
      background: false,
      unique: true,
    },
  },
  paymentAmount: {
    type: String,
    required: true,
  },
  paid: {
    type: Boolean,
    required: true,
    default: false,
  },
  memberIds: {
    type: [String],
    default: [],
  },
});

const teamModel = mongoose.model('Team', TeamSchema, 'teams');

module.exports = {
  teamModel,
};
