const mongoose = require('mongoose');


const MemberSchema = new mongoose.Schema({
  firstName: {
    type: String,
    required: true,
  },
  lastName: {
    type: String,
    required: true,
  },
  isHead: {
    type: Boolean,
    required: true,
    default: false,
  },
  mobile: {
    type: String,
    required: true,
    unique: true,
    index: {
      background: false,
      unique: true,
    },
  },
  studentId: {
    type: String,
    required: true,
    unique: true,
    index: {
      background: false,
      unique: true,
    },
  },
  nationalId: {
    type: String,
    required: true,
    unique: true,
    index: {
      background: false,
      unique: true,
    },
  },
  teamId: {
    type: String,
    required: true,
  },
  joinedAt: {
    type: String,
    required: true,
  },
  password: {
    type: String,
    required: false,
    default: '',
  },
});

const memberModel = mongoose.model('Member', MemberSchema, 'members');

module.exports = {
  memberModel,
};
