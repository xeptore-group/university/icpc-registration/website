const mongoose = require('mongoose');


const TransactionSchema = new mongoose.Schema({
  description: {
    type: String,
    required: false,
    default: '',
  },
  createdAt: {
    type: String,
    required: true,
    unique: true,
  },
  verifiedAt: {
    type: String,
    required: false,
    default: '',
  },
  verificationStatus: {
    type: String,
    required: false,
    default: '',
  },
  paymentAmount: {
    type: String,
    required: true,
  },
  mobile: {
    type: String,
    required: true,
  },
  teamId: {
    type: String,
    required: true,
  },
  authority: {
    type: String,
    required: false,
    default: '',
  },
  refId: {
    type: String,
    required: false,
    default: '',
  },
});

const transactionModel = mongoose.model(
  'Transaction',
  TransactionSchema,
  'transactions',
);

module.exports = transactionModel;
