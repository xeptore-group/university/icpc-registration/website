const mongoose = require('mongoose');


const PaymentSchema = new mongoose.Schema({
  teamId: {
    type: String,
    required: true,
  },
  orderId: {
    type: String,
    required: true,
    index: {
      unique: true,
      background: false,
    },
  },
  iid: {
    type: String,
    required: true,
    index: {
      unique: true,
      background: false,
    },
  },
  link: {
    type: String,
    required: true,
    index: {
      unique: true,
      background: false,
    },
  },
  amount: {
    type: String,
    required: true,
  },
  name: {
    type: String,
    required: true,
  },
  phone: {
    type: String,
    required: true,
  },
  description: {
    type: String,
    required: true,
  },
  trackId: {
    type: String,
  },
  cardNo: {
    type: String,
  },
  hashedCardNo: {
    type: String,
  },
  paidAt: {
    type: String,
  },
  verified: {
    type: Boolean,
    required: true,
    default: false,
  },
  verifiedAt: {
    type: String,
  },
});

module.exports = mongoose.model('Payment', PaymentSchema, 'payments');
