const pino = require('pino');
const config = require('./lib/config');


const loggerConf = config.logging.simple;

const serializers = {
  req(req) {
    return loggerConf.verbose ? {
      method: req.method,
      url: req.url,
      params: req.params,
      query: req.query,
      headers: req.headers,
      id: req.id,
      ip: req.ip,
      ips: req.ips,
      hostname: req.hostname,
    } : {
      url: req.url,
    };
  },
  res(res) {
    return {
      statusCode: res.statusCode,
    };
  },
};

const logger = pino({
  timestamp: true,
  prettyPrint: !loggerConf.file ? {
    levelFirst: true,
    colorize: true,
    translateTime: 'm/d/yy HH:MM:ss',
  } : false,
  serializers,
  enabled: loggerConf.enabled,
}, loggerConf.file ? pino.destination(loggerConf.logFileName) : process.stdout);

module.exports = logger;
