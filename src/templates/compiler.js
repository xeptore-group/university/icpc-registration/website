const pug = require('pug');
const { resolve } = require('path');


function path(templateName) {
  return resolve(__dirname, `${templateName}.pug`);
}

function compile(templateName) {
  return pug.compileFile(path(templateName), {
    basedir: resolve(__dirname),
    filename: templateName,
  });
}

const templates = {
  login: compile('login'),
  signup: compile('signup'),
  dashboard: compile('dashboard'),
  internalError: compile('internal-error'),
  successPayment: compile('success-payment'),
  failPayment: compile('fail-payment'),
  fourOhFour: compile('four-oh-four'),
};

module.exports = {
  ...templates,
};
