const { fourOhFour } = require('../templates/compiler');


function handler(request, reply) {
  reply.html(fourOhFour());
}


module.exports = handler;
