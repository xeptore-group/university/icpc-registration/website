const config = require('../lib/config');
const { UNAUTHORIZED } = require('http-status-codes');
const { login: template } = require('../templates/compiler');
const { findHeadMemberWithPassword } = require('../repos/members');
const { passwordManager, jwt } = require('../services');
const { handleInternalError } = require('./helpers');


function getLogin(requets, reply) {
  reply.html(template({}));
}

async function postLogin(request, reply) {
  const form = request.body;
  const username = form.username;
  const password = form.password;

  const [member, errFindMember] = await findHeadMemberWithPassword(username);
  if (errFindMember) {
    const err = {
      error: errFindMember,
      operation: 'finding member by national id.',
      payload: {
        nationalId: username,
      },
    };
    handleInternalError(request, reply, err);
    return;
  } else if (!member) {
    reply.code(UNAUTHORIZED).html(template({
      error: {
        message: 'Invalid username/password',
      },
    }));
    return;
  }

  const [verified, errVer] = await passwordManager.verify(password, member.password);
  if (errVer) {
    const err = {
      error: errFindMember,
      operation: 'verifying member password.',
      payload: {
        username,
        password,
      },
    };
    handleInternalError(request, reply, err);
    return;
  } else if (!verified) {
    reply.code(UNAUTHORIZED).html(template({
      error: {
        message: 'Invalid username/password',
      },
    }));
    return;
  }

  const [token, errToken] = await jwt.signToken({
    userId: member.id,
  });
  if (errToken) {
    const err = {
      error: errToken,
      operation: 'signing token',
      payload: {
        username: member.id,
      },
    };
    handleInternalError(request, reply, err);
    return;
  }

  reply.setCookie('icpc_auth', token, {
    domain: config.server.domain,
    path: '/',
    secure: false,
    httpOnly: false,
    expires: new Date(Date.now() + 1000 * 60 * 60 * 24 * 10),
  }).redirect('/dashboard');
}

module.exports = {
  getLogin,
  postLogin,
};
