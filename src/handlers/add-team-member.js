const { memberExists, registerMemeber } = require('../repos/members');
const {
  addTeamMember,
  teamExists,
  getTeamMembersCount,
  updateTeamPaymentAmount,
  getTeamById,
} = require('../repos/teams');
const { calculateTeamPaymentAmount, ErrorTypes } = require('./helpers');
const validator = require('../services/validator');
const schema = require('../schemes/add-team-member.schema');


async function handler(request) {
  const form = request.body;
  const validationRes = validator(schema)(form);
  if (!validationRes.isValid) {
    return [true, {
      type: ErrorTypes.Validation,
      error: validationRes.errors,
    }];
  }

  const teamId = request.body.teamId;

  const [doesTeamExists, errTeamExist] = await teamExists(teamId);
  if (errTeamExist) {
    const err = {
      error: errTeamExist,
      operation: 'checking whether team exists before adding member to it.',
      payload: {
        teamId: teamId,
      },
    };
    return [false, err];
  } else if (!doesTeamExists) {
    return [true, {
      type: ErrorTypes.NotFound,
    }];
  }

  const [team, errFindTeam] = await getTeamById(teamId);
  if (errFindTeam) {
    const err = {
      error: errFindTeam,
      operation: 'fetching team information in add-memeber',
      payload: {
        teamId: teamId,
      },
    };
    return [false, err];
  } else if (!team) {
    return [true, {
      type: ErrorTypes.NotFound,
    }];
  }

  // TODO: check here why do we get duplicate mobile error...
  const [exists, errExists] = await memberExists({
    nationalId: form.nationalId,
    mobile: form.mobile,
    studentId: form.studentId,
  });
  if (errExists) {
    const err = {
      error: errExists,
      operation: 'checking whether member exists before adding him to a team',
      payload: {
        teamId: teamId,
        member: form,
      },
    };
    return [false, err];
  } else if (exists) {
    return [true, {
      type: ErrorTypes.Duplicate,
      error: {
        message: 'Member already exists.',
      },
    }];
  }

  const member = {
    isHead: false,
    teamId,
    firstName: form.firstName,
    lastName: form.lastName,
    nationalId: form.nationalId,
    studentId: form.studentId,
    mobile: form.mobile,
    password: '',
    joinedAt: new Date().toISOString(),
  };
  const [savedMember, errSavingMember] = await registerMemeber(member);
  if (errSavingMember) {
    const err = {
      error: errSavingMember,
      operation: 'saving member.',
      payload: {
        teamId,
        member,
      },
    };
    return [false, err];
  }

  const [prevMembersCount, errGetMembersCount] = await getTeamMembersCount(teamId);
  if (errGetMembersCount) {
    const err = {
      error: errGetMembersCount,
      operation: 'fetching previous count of members of a team.',
      payload: {
        teamId,
      },
    };
    return [false, err];
  } else if (prevMembersCount >= 3) {
    return [true, undefined];
  }

  const errAddMember = await addTeamMember(teamId, savedMember.id);
  if (errAddMember) {
    const err = {
      error: errAddMember,
      operation: 'assigning user to a team after saving him',
      payload: {
        teamId,
        savedMember,
      },
    };
    return [false, err];
  }

  const [newMembersCount, errMembersCount] = await getTeamMembersCount(teamId);
  if (errMembersCount) {
    const err = {
      error: errMembersCount,
      operation: 'fetching count of new members of a team',
      payload: {
        teamId,
      },
    };
    return [false, err];
  }

  const newPaymentAmount = calculateTeamPaymentAmount(newMembersCount);
  const errUpdatePayAmount = await updateTeamPaymentAmount(teamId, newPaymentAmount);
  if (errUpdatePayAmount) {
    const err = {
      error: errUpdatePayAmount,
      operation: 'updating team with new payment amount due to count of members change',
      payload: {
        teamId,
        prevMembersCount,
        newMembersCount,
        newPaymentAmount,
      },
    };
    return [false, err];
  }

  return [true, undefined];
}

module.exports = handler;
