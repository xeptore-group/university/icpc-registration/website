const { home } = require('./home');
const { getLogin, postLogin } = require('./login');
const { getSignup, postSignup } = require('./signup');
const { dashboard, postDashboard } = require('./dashboard');
const addTeamMemberHandler = require('./add-team-member');
const deleteTeamMemberHandler = require('./delete-team-member');
const internalErrorHandler = require('./internal-error');
const { redirectHandler, requestHandler } = require('./idpay-payment');
const fourOhFourHandler = require('./four-oh-four');


module.exports = {
  home,
  getLogin,
  postLogin,
  getSignup,
  postSignup,
  dashboard,
  addTeamMemberHandler,
  deleteTeamMemberHandler,
  internalErrorHandler,
  paymentHandler: requestHandler,
  paymentRedirectHandler: redirectHandler,
  fourOhFourHandler,
  postDashboard,
};
