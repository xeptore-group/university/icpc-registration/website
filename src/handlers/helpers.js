const { logger } = require('../services');


const handleInternalError = (request, reply, error) => {
  logger.error({
    message: error.error,
    operation: error.operation,
    payload: error.payload,
    request,
  }, 'an internal error occurred');

  reply.redirect('/internal-error');
};

function calculateTeamPaymentAmount(membersCount) {
  switch (membersCount) {
    case 2: {
      return 500000;
    }
    case 3: {
      return 500000;
    }
    default: {
      return 500000;
    }
  }
}

const ErrorTypes = {
  Internal: 'internal',
  NotFound: 'not_found',
  Duplicate: 'duplicate',
  Validation: 'validation',
};

module.exports = {
  handleInternalError,
  calculateTeamPaymentAmount,
  ErrorTypes,
};
