function home(request, reply) {
  reply.redirect('/dashboard');
}

module.exports = {
  home,
};
