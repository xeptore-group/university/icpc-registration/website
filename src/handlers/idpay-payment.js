const _ = require('lodash');
const uuid = require('uuid/v4');
const { payment: paymentConfig } = require('../lib/config');
const { getTeamById, setTeamPaid } = require('../repos/teams');
const { getMemberById } = require('../repos/members');
const paymentRepo = require('../repos/payment');
const { handleInternalError } = require('./helpers');
const { authenticate } = require('../services/auth');
const { failPayment, successPayment } = require('../templates/compiler');
const persistLogger = require('../services/logger');


const requestHandler = (idpay) => async (request, reply) => {
  if (!paymentConfig.enabled) {
    reply.redirect('/dashboard');
    return;
  }

  const { icpc_auth: token } = request.cookies;
  const [decode, errVerify] = await authenticate(token);
  if (errVerify || !decode) {
    reply.redirect('/auth/login');
    return;
  }

  const { teamId } = request.params;
  const [team, errGetTeam] = await getTeamById(teamId);
  if (errGetTeam) {
    const error = {
      error: errGetTeam,
      operation: 'fetching team details in payment.',
      payload: {
        teamId,
      },
    };
    handleInternalError(request, reply, error);
    return;
  } else if (team === null) {
    reply.redirect('/auth/signup');
    return;
  } else if (team.paid) {
    reply.redirect('/dashboard');
    return;
  }

  const [member, errGetMember] = await getMemberById(decode.id);
  if (errGetMember) {
    const error = {
      error: errGetMember,
      operation: 'fetching member details.',
      payload: {
        memberId: member.id,
      },
    };
    handleInternalError(request, reply, error);
    return;
  } else if (member === null) {
    reply.redirect('/auth/signup');
    return;
  }

  const amount = team.paymentAmount;
  const description = `${paymentConfig.description} تیم: ${team.name}`;
  const mobile = member.mobile;

  const paymentUuid = uuid();
  const payment = {
    amount,
    description,
    phone: mobile,
    mail: 'mbehboodian98@gmail.com',
    orderId: paymentUuid,
    name: `${member.firstName} ${member.lastName}`,
  };
  const [idpayRes, errLinkRequest] = await idpay.request(payment);
  if (errLinkRequest) {
    const error = {
      error: errLinkRequest,
      operation: 'requesting payment url from idpay.',
      payoad: {
        paymentDetails: payment,
      },
    };
    handleInternalError(request, reply, error);
    return;
  }

  const [, errPersistPayment] = await paymentRepo.persist({
    teamId: team.id,
    amount: payment.amount,
    description: payment.description,
    iid: idpayRes.id,
    link: idpayRes.link,
    name: payment.name,
    orderId: payment.orderId,
    phone: payment.phone,
  });
  if (errPersistPayment) {
    const error = {
      error: errPersistPayment,
      operation: 'persisting payment details.',
      payoad: {
        paymentDetails: payment,
      },
    };
    handleInternalError(request, reply, error);
    return;
  }

  reply.redirect(idpayRes.link);
};


const redirectHandler = (idpay) => async (request, reply) => {
  const {
    status,
    track_id: trackId,
    id,
    order_id: orderId,
    card_no: cardNo,
    hashed_card_no: hashedCardNo,
    date,
  } = request.body;

  persistLogger.debug('idpay.callback.received', {
    request: _.omit(request, ['raw', 'req', 'log']),
  });

  if (status !== '10') {
    reply.html(failPayment({
      paymentError: true,
      internalError: false,
    }));
    return;
  }

  const [
    paymentExists,
    errPaymentExists,
  ] = await paymentRepo.verifiedPaymentExists({ trackId, iid: id });
  if (errPaymentExists) {
    const error = {
      error: errPaymentExists,
      operation: 'checking payment existance in idpay callback.',
      payload: {
        requestBody: request.body,
      },
    };
    handleInternalError(request, reply, error);
    return;
  } else if (paymentExists) {
    reply.html(failPayment({
      paymentError: true,
      internalError: false,
    }));
    return;
  }

  const [payment, errGetPayment] = await paymentRepo.getByOrderId(orderId);
  if (errGetPayment) {
    const error = {
      error: errGetPayment,
      operation: 'fetching payment details in idpay callback.',
      payload: {
        requestBody: request.body,
      },
    };
    handleInternalError(request, reply, error);
    return;
  } else if (payment === null) {
    const error = {
      error: new Error('payment not found'),
      operation: 'unable to find payment for verification.',
      payload: {
        requestBody: request.body,
      },
    };
    handleInternalError(request, reply, error);
    return;
  }

  const verificationParams = { id, orderId };

  const [result, errVerification] = await idpay.verify(verificationParams);
  if (errVerification) {
    const error = {
      error: errVerification,
      operation: 'verifying payment from idpay.',
      payload: {
        rrequestParams: verificationParams,
      },
    };
    handleInternalError(request, reply, error);
    return;
  }

  const errUpdate = await paymentRepo.update({
    id: payment.id,
    trackId,
    cardNo,
    hashedCardNo,
    paidAt: date,
    verified: true,
    verifiedAt: result.verify.date,
  });
  if (errUpdate) {
    const error = {
      error: errUpdate,
      operation: 'updating payment info.',
      payload: {
        details: {
          request: _.omit(request, ['raw', 'req', 'log']),
          verificationResult: result,
        },
      },
    };
    handleInternalError(request, reply, error);
    return;
  }

  const errUpdateTeamPaid = await setTeamPaid(payment.teamId);
  if (errUpdateTeamPaid) {
    const error = {
      error: errUpdateTeamPaid,
      operation: 'setting team paid to true.',
      payload: {
        paymentDetails: payment,
      },
    };
    handleInternalError(request, reply, error);
    return;
  }

  reply.html(successPayment({
    trackNo: trackId,
  }));
};

module.exports = {
  requestHandler,
  redirectHandler,
};
