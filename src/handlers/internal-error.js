const { internalError: template } = require('../templates/compiler');


function handler(request, reply) {
  reply.html(template());
}

module.exports = handler;
