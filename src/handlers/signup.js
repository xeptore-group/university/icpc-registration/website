const { CREATED, BAD_REQUEST } = require('http-status-codes');
const { passwordManager } = require('../services');
const { signup: template } = require('../templates/compiler');
const {
  memberExists,
  registerMemeber,
  assignMemberToTeam,
} = require('../repos/members');
const {
  isTeamNameUnique,
  registerTeam,
} = require('../repos/teams');
const { handleInternalError } = require('./helpers');
const { validator } = require('../services');
const { signupFormSchema } = require('../schemes');


function getSignup(requets, reply) {
  reply.html(template());
}

async function postSignup(request, reply) {
  const form = request.body;

  const validationResult = validator(signupFormSchema)(form);
  if (!validationResult.isValid) {
    reply.code(BAD_REQUEST).html(template({
      validation: {
        errors: validationResult.errors,
      },
      form,
    }));
    return;
  }

  const [exists, e1] = await memberExists({
    nationalId: form.nationalId,
    mobile: form.mobile,
    studentId: form.studentId,
  });
  if (e1) {
    const err = {
      error: e1,
      operation: 'checking member exists',
      payload: {
        nationalId: form.nationalId,
        mobile: form.mobile,
        studentId: form.studentId,
      },
    };
    handleInternalError(request, reply, err);
    return;
  } else if (exists) {
    reply.code(BAD_REQUEST).html(template({
      error: {
        message: 'User already registered.',
      },
      form,
    }));
    return;
  }

  const teamName = form.teamName;
  const [teamExists, e2] = await isTeamNameUnique(teamName);
  if (e2) {
    const err = {
      error: err,
      operation: 'checking team name uniquness.',
      payload: {
        teamName,
      },
    };
    handleInternalError(request, reply, err);
    return;
  } else if (teamExists) {
    reply.code(BAD_REQUEST).html(template({
      error: {
        message: `Team name "${teamName}" already exists.`,
      },
      form,
    }));
    return;
  }

  const member = {
    firstName: form.firstName,
    lastName: form.lastName,
    isHead: true,
    nationalId: form.nationalId,
    studentId: form.studentId,
    mobile: form.mobile,
    teamId: teamName,
    password: '',
    joinedAt: new Date().toISOString(),
  };

  const [password, ePass] = await passwordManager.hash(member.nationalId);
  if (ePass) {
    const err = {
      error: ePass,
      operation: 'hashing team head member password.',
      payload: {
        member,
      },
    };
    handleInternalError(request, reply, err);
    return;
  }
  member.password = password;

  const [savedMember, e3] = await registerMemeber(member);
  if (e3) {
    const err = {
      error: e3,
      operation: 'registering member.',
      payload: {
        member,
      },
    };
    handleInternalError(request, reply, err);
    return;
  }

  const team = {
    name: form.teamName,
    registeredAt: new Date().toISOString(),
    registrarId: savedMember.id,
    paymentAmount: 500000,
    paid: false,
    memberIds: [savedMember.id],
  };
  const [savedTeam, e4] = await registerTeam(team);
  if (e4) {
    const err = {
      error: e4,
      operation: 'registering team.',
      payload: {
        team,
      },
    };
    handleInternalError(request, reply, err);
    return;
  }

  const e5 = await assignMemberToTeam(savedMember.id, savedTeam.id);
  if (e5) {
    const err = {
      error: e5,
      operation: 'assigning member to team.',
      payload: {
        member: savedMember,
        team: savedTeam,
      },
    };
    handleInternalError(request, reply, err);
    return;
  }

  reply.code(CREATED).html(template({
    success: {
      team: savedTeam,
    },
  }));
}

module.exports = {
  getSignup,
  postSignup,
};
