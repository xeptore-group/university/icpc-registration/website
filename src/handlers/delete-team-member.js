const { deleteMember } = require('../repos/members');
const {
  teamExists,
  getTeamMembersCount,
  updateTeamPaymentAmount,
  deleteTeamMember,
  getTeamById,
} = require('../repos/teams');
const { calculateTeamPaymentAmount, ErrorTypes } = require('./helpers');


async function handler(request) {
  const form = request.body;
  const teamId = form.teamId;
  const memberId = form.memberId;

  const [doesTeamExists, errTeamExist] = await teamExists(teamId);
  if (errTeamExist) {
    const err = {
      error: errTeamExist,
      operation: 'verifying team existance before deleting a member from it.',
      payload: {
        teamId,
      },
    };
    return [false, err];
  } else if (!doesTeamExists) {
    return [true, {
      type: ErrorTypes.NotFound,
    }];
  }

  const [team, errFindTeam] = await getTeamById(teamId);
  if (errFindTeam) {
    const err = {
      error: errFindTeam,
      operation: 'fetching team information in delete-memeber',
      payload: {
        teamId,
      },
    };
    return [false, err];
  } else if (!team) {
    return [true, {
      type: ErrorTypes.NotFound,
    }];
  }

  const [prevMembersCount, errGetMembersCount] = await getTeamMembersCount(teamId);
  if (errGetMembersCount) {
    const err = {
      error: errGetMembersCount,
      operation: 'fetching previous count of members of a team.',
      payload: {
        teamId,
      },
    };
    return [false, err];
  } else if (prevMembersCount <= 1) {
    return [true, undefined];
  }

  const errDeleteMember = await deleteMember(memberId);
  if (errDeleteMember) {
    const err = {
      error: errDeleteMember,
      operation: 'deleting a member from database.',
      payload: {
        teamId,
        memberId,
      },
    };
    return [false, err];
  }

  const errDeleteTeamMember = await deleteTeamMember(teamId, memberId);
  if (errDeleteTeamMember) {
    const err = {
      error: errDeleteTeamMember,
      operation: 'unassigning deleted member from a his team.',
      payload: {
        teamId,
        memberId,
      },
    };
    return [false, err];
  }

  const [newMembersCount, errMembersCount] = await getTeamMembersCount(teamId);
  if (errMembersCount) {
    const err = {
      error: errMembersCount,
      operation: 'fetching count of new members of a team.',
      payload: {
        teamId,
      },
    };
    return [false, err];
  }

  const newPaymentAmount = calculateTeamPaymentAmount(newMembersCount);
  const errUpdatePayAmount = await updateTeamPaymentAmount(teamId, newPaymentAmount);
  if (errUpdatePayAmount) {
    const err = {
      error: errUpdatePayAmount,
      operation: 'updating team with new payment amount due to count of members change',
      payload: {
        teamId,
        newMembersCount,
        newPaymentAmount,
      },
    };
    return [false, err];
  }

  return [true, undefined];
}

module.exports = handler;

