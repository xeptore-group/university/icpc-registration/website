const { payment: paymentConfig } = require('../lib/config');
const { getTeamById, setTeamPaid } = require('../repos/teams');
const { getMemberById } = require('../repos/members');
const transactionRepository = require('../repos/transactions');
const { handleInternalError } = require('./helpers');
const { authenticate } = require('../services/auth');
const { ZarinpalErrors } = require('../services/zarinpal');
const { failPayment, successPayment } = require('../templates/compiler');


const requestHandler = (zarinpal) => async (request, reply) => {
  if (!paymentConfig.enabled) {
    reply.redirect('/dashboard');
    return;
  }

  const { icpc_auth: token } = request.cookies;
  const [decode, errVerify] = await authenticate(token);
  if (errVerify || !decode) {
    reply.redirect('/auth/login');
    return;
  }

  const { teamId } = request.params;
  const [team, errGetTeam] = await getTeamById(teamId);
  if (errGetTeam) {
    const error = {
      error: errGetTeam,
      operation: 'fetching team details in payment.',
      payload: {
        teamId,
      },
    };
    handleInternalError(request, reply, error);
    return;
  } else if (team === null) {
    reply.redirect('/auth/signup');
    return;
  } else if (team.paid) {
    reply.redirect('/dashboard');
    return;
  }

  const [member, errGetMember] = await getMemberById(decode.id);
  if (errGetMember) {
    const error = {
      error: errGetMember,
      operation: 'fetching member details.',
      payload: {
        memberId: member.id,
      },
    };
    handleInternalError(request, reply, error);
    return;
  } else if (member === null) {
    reply.redirect('/auth/signup');
    return;
  }

  const amount = team.paymentAmount;
  const description = `${paymentConfig.description} تیم: ${team.name}`;
  const mobile = member.mobile;

  const [transaction, errCreatingTransaction] = await transactionRepository.create({
    description,
    mobile,
    paymentAmount: amount,
    authority: '',
    teamId: team.id,
  });
  if (errCreatingTransaction) {
    const details = {
      description,
      mobile,
      amount,
      teamId,
    };
    const error = {
      error: errCreatingTransaction,
      operation: 'creating transaction.',
      payload: {
        details,
      },
    };
    handleInternalError(request, reply, error);
    return;
  }

  const payment = {
    amount,
    description,
    phone: mobile,
    email: '',
    transactionId: transaction.id,
  };

  const [result, errZarinpal] = await zarinpal.request(payment);
  if (errZarinpal) {
    const error = {
      error: errZarinpal,
      operation: 'unknown error while requesting payment url from zarinpal.',
      payoad: {
        paymentDetails: payment,
      },
    };
    handleInternalError(request, reply, error);
    return;
  } else if (result.error !== undefined) {
    const error = {
      error: errZarinpal,
      operation: 'zarinpal error while requesting payment url from zarinpal.',
      payoad: {
        paymentDetails: payment,
        zarinpalResponse: result,
      },
    };
    handleInternalError(request, reply, error);
    return;
  }

  const errUpdatingAuthority = await transactionRepository.updateAuthority({
    transactionId: transaction.id,
    authority: result.authority,
  });
  if (errUpdatingAuthority) {
    const error = {
      error: errUpdatingAuthority,
      operation: 'updating transaction authority.',
      payload: {
        transaction,
        zarinpalResponse: result,
      },
    };
    handleInternalError(request, reply, error);
    return;
  }

  reply.redirect(result.url);
};

const redirectHandler = (zarinpal) => async (request, reply) => {
  const {
    Status: status,
    Authority: authority,
    transaction_id: transactionId,
  } = request.query;

  const [transaction, errGetTransaction] = await transactionRepository
    .getById(transactionId);
  if (errGetTransaction) {
    const error = {
      error: errGetTransaction,
      operation: 'fetching transaction details in zarinpal callback.',
      payload: {
        transactionId,
        authority,
      },
    };
    handleInternalError(request, reply, error);
    return;
  } else if (transaction === null) {
    const error = {
      error: new Error('transaction not found'),
      operation: 'unable to find transaction for verification.',
      payload: {
        transactionId,
        authority,
      },
    };
    handleInternalError(request, reply, error);
    return;
  }

  const { paymentAmount: amount, teamId } = transaction;

  const verificationParams = {
    status,
    amount,
    authority,
  };

  const [result, errVerification] = await zarinpal.verify(verificationParams);
  if (errVerification) {
    if (errVerification === ZarinpalErrors.Failed) {
      reply.redirect(failPayment({
        paymentError: true,
        internalError: false,
      }));
      return;
    }

    const error = {
      error: errVerification,
      operation: 'unknown error while verifying payment from zarinpal. it might be an issue with zarinpal.',
      payload: {
        teamId,
        transactionId,
        zarinpalVerificationRequestParams: verificationParams,
      },
    };
    handleInternalError(request, reply, error);
    return;
  } else if (result.error !== undefined) {
    reply.html(failPayment({
      paymentError: false,
      internalError: true,
    }));

    return;
  }

  const errUpdateTeamPaid = await setTeamPaid(teamId);
  if (errUpdateTeamPaid) {
    const error = {
      error: errUpdateTeamPaid,
      operation: 'setting team paid to true.',
      payload: {
        teamId,
        transactionId,
      },
    };
    handleInternalError(request, reply, error);
    reply.html(successPayment({
      error: {
        message: 'Payment succeeded, but there were an internal error. call the administrator',
      },
    }));
    return;
  }

  const errVerfyTransaction = await transactionRepository.verify({
    refId: result.refId,
    transactionId,
    verificationStatus: 100,
    verifiedAt: new Date().toISOString(),
  });
  if (errVerfyTransaction) {
    const error = {
      error: errVerfyTransaction,
      operation: 'updating local transaction to set it verified.',
      payload: {
        teamId,
        transactionId,
        zarinpalVerificationDetails: result,
      },
    };
    handleInternalError(request, reply, error);
    reply.html(successPayment({
      error: {
        message: 'Payment succeeded, but there were an internal error. call the administrator',
      },
    }));
    return;
  }

  reply.html(successPayment({
    trackNo: result.refId,
  }));
};

module.exports = {
  requestHandler,
  redirectHandler,
};
