const config = require('../lib/config');
const { dashboard: template } = require('../templates/compiler');
const { auth } = require('../services');
const { getMemberTeam } = require('../repos/members');
const { getTeamMembers } = require('../repos/teams');
const { handleInternalError, ErrorTypes } = require('./helpers');
const addTeamMemberHandler = require('./add-team-member');
const deleteTeamMemberHandler = require('./delete-team-member');


async function dashboard(request, reply) {
  const token = request.cookies.icpc_auth;
  const [decode, errVerify] = await auth.authenticate(token);
  if (errVerify || !decode) {
    reply.redirect('/auth/login');
    return;
  }

  const [team, errFindTeam] = await getMemberTeam(decode.id);
  if (errFindTeam) {
    const err = {
      error: errFindTeam,
      operation: 'finding head member team.',
      payload: {
        headMemberId: decode.id,
      },
    };
    handleInternalError(request, reply, err);
    return;
  } else if (!team) {
    reply.redirect('/auth/signup');
    return;
  }

  const [members, errFindMembers] = await getTeamMembers(team.id);
  if (errFindMembers) {
    const err = {
      error: errFindMembers,
      operation: 'finding team members.',
      payload: {
        teamId: team.id,
      },
    };
    handleInternalError(request, reply, err);
    return;
  }

  reply.html(template({
    team: {
      members,
      name: team.name,
      id: team.id,
      paid: team.paid,
      paymentAmount: team.paymentAmount,
      canAddMoreMemebers: members.length < 3,
      canDeleteMemeber: members.length > 1,
    },
    error: request.query.error,
    loggedIn: true,
    paymentEnabled: config.payment.enabled,
  }));
}

async function postDashboard(request, reply) {
  const token = request.cookies.icpc_auth;
  const [decode, errVerify] = await auth.authenticate(token);
  if (errVerify || !decode) {
    reply.redirect('/auth/login');
    return;
  }

  let templateError;
  let form;

  switch (request.body.action) {
    case ('addMember'): {
      const [ok, error] = await addTeamMemberHandler(request);
      if (!ok) {
        handleInternalError(request, reply, error);
        return;
      } else if (ok && error !== undefined) {
        if (error.type === ErrorTypes.NotFound) {
          reply.redirect('/auth/signup');
        } else if (error.type === ErrorTypes.Validation) {
          templateError = {
            validation: error.error,
          };
          form = request.body;
        } else if (error.type === ErrorTypes.Duplicate) {
          templateError = error.error;
        }
      }
      break;
    }
    case ('deleteMember'): {
      const [ok, error] = await deleteTeamMemberHandler(request);
      if (!ok) {
        handleInternalError(request, reply, error);
        return;
      } else if (ok && error !== undefined) {
        if (error.type === ErrorTypes.NotFound) {
          reply.redirect('/auth/signup');
        }
      }
      break;
    }
    default: {
      break;
    }
  }

  const [team, errFindTeam] = await getMemberTeam(decode.id);
  if (errFindTeam) {
    const err = {
      error: errFindTeam,
      operation: 'finding head member team.',
      payload: {
        headMemberId: decode.id,
      },
    };
    handleInternalError(request, reply, err);
    return;
  } else if (!team) {
    reply.redirect('/auth/signup');
    return;
  }

  const [members, errFindMembers] = await getTeamMembers(team.id);
  if (errFindMembers) {
    const err = {
      error: errFindMembers,
      operation: 'finding team members.',
      payload: {
        teamId: team.id,
      },
    };
    handleInternalError(request, reply, err);
    return;
  }

  reply.html(template({
    team: {
      members,
      name: team.name,
      id: team.id,
      paid: team.paid,
      paymentAmount: team.paymentAmount,
      canAddMoreMemebers: members.length < 3,
      canDeleteMemeber: members.length > 1,
    },
    error: templateError,
    form,
    loggedIn: true,
    paymentEnabled: config.payment.enabled,
  }));
}

module.exports = {
  dashboard,
  postDashboard,
};
