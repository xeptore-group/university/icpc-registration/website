const {
  required,
  typeString,
  trimmed,
  notEmpty,
  length,
  typeNumber,
  mustMatch,
} = require('../services/validators');


const signupFormSchema = {
  firstName: {
    validator(x) {
      return required(x)
        .chain(typeString())
        .chain(trimmed())
        .chain(notEmpty())
        .fold()[0];
    },
  },
  lastName: {
    validator(x) {
      return required(x)
        .chain(typeString())
        .chain(trimmed())
        .chain(notEmpty())
        .fold()[0];
    },
  },
  studentId: {
    validator(x) {
      return required(x)
        .chain(typeString())
        .chain(trimmed())
        .chain(notEmpty())
        .chain(mustMatch('student id format', /^9[3-6][0-9]{8}$/))
        .chain(length(10))
        .fold()[0];
    },
  },
  mobile: {
    validator(x) {
      return required(x)
        .chain(trimmed())
        .chain(notEmpty())
        .chain(typeNumber())
        .chain(mustMatch('mobile number format', /^9[0-9]{9}$/))
        .fold()[0];
    },
  },
  nationalId: {
    validator(x) {
      return required(x)
        .chain(typeString())
        .chain(trimmed())
        .chain(notEmpty())
        .chain(mustMatch('national id format', /^[0-9]{10}$/))
        .chain(length(10))
        .fold()[0];
    },
  },
  teamName: {
    validator(x) {
      return required(x)
        .chain(typeString())
        .chain(trimmed())
        .chain(notEmpty())
        .fold()[0];
    },
  },
};

module.exports = signupFormSchema;
