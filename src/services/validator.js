const validator = (schema) => (form) => {
  const errors = {};
  Object.keys(schema).forEach((prop) => {
    errors[prop] = schema[prop].validator(form[prop]);
  });

  const res = {
    isValid: true,
    errors: filterEmptyFieldErrors(errors),
  };

  if (Object.keys(res.errors).length) {
    res.isValid = false;
  } else {
    res.errors = null;
  }

  return res;
};

function filterEmptyFieldErrors(errors) {
  return Object.keys(errors)
    .filter((key) => !!errors[key])
    .map((key) => ({ [key]: errors[key] }))
    .reduce((prev, curr) => ({
      ...prev,
      ...curr,
    }), {});
}

module.exports = validator;
