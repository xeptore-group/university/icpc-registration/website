const Right = (x) => ({
  chain: (fn) => fn(x),
  fold: () => [undefined, x],
});

const Left = (x) => ({
  chain: () => Left(x),
  fold: () => [x],
});


const notExists = (x) => x === null || x === undefined;

const exists = (x) => !notExists(x);

const required = (x) => exists(x) ? Right(x) : Left('required');

const nonRequired = (x) => exists(x) ? Right(x) : Left(undefined);

const optional = (x) => nonRequired(x);

const oneOf = (possibilities) => (x) => possibilities.includes(x) ?
  Right(x) :
  Left('must match one of expected values');

const trimmed = () => (x) => Right(x.trim());

const length = (l) =>
  (x) => x.length === l ? Right(x) : Left(`must have exactly length of ${l}`);

const minLength = (l) =>
  (x) => x.length >= l ? Right(x) : Left(`must have at least length of ${l}`);

const maxLength = (l) =>
  (x) => x.length <= l ? Right(x) : Left(`must have at most length of ${l}`);

const notEmpty = () => (x) => x.trim().length !== 0 ?
  Right(x) :
  Left('cannot be empty');

const ofType = (
  type,
  ...additionalValidators) =>
  (x) => typeof x === type && additionalValidators.map((v) => v(x)).every((v) => v)
    ? Right(x)
    : Left(`must be of type ${type}`);

const typeString = () => ofType('string');

const typeNumber = () => (x) => ofType('number')(parseInt(x));

const mustMatch = (name, regex) => (x) => regex.test(x) ?
  Right(x) :
  Left(`must match ${name}`);

module.exports = {
  required,
  optional,
  typeString,
  trimmed,
  notEmpty,
  length,
  minLength,
  maxLength,
  mustMatch,
  typeNumber,
  oneOf,
};
