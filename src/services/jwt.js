const jwt = require('jsonwebtoken');
const uuid = require('uuid/v4');
const { promisify } = require('util');


const sign = promisify(jwt.sign);
const verify = promisify(jwt.verify);
const SECRET = process.env.JWT_SECRET;
const ISSUER = 'auth.acm.birjandut.ac.ir';

async function signToken({ userId }) {
  try {
    const token = await sign({
      id: userId,
    }, SECRET, {
      algorithm: 'HS512',
      expiresIn: '10 days',
      issuer: ISSUER,
      jwtid: uuid(),
      subject: userId,
    });

    return [token, undefined];
  } catch (error) {
    return [undefined, error];
  }
}

async function verifyToken(token) {
  try {
    const decode = await verify(token, SECRET, {
      algorithms: ['HS512'],
      issuer: ISSUER,
    });

    return [decode, undefined];
  } catch (error) {
    return [undefined, error];
  }
}


module.exports = {
  signToken,
  verifyToken,
};

