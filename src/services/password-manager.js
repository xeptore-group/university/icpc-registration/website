const crypto = require('crypto');
const { promisify } = require('util');


const randomBytes = promisify(crypto.randomBytes);
const pbkdf2 = promisify(crypto.pbkdf2);

const digest = 'sha512';
const iterations = 999999;
const keyLength = 256;

async function hash(password) {
  try {
    const salt = await randomBytes(keyLength);
    const key = await pbkdf2(password, salt, iterations, keyLength, digest);

    const buffer = Buffer.alloc(keyLength * 2);

    salt.copy(buffer);
    key.copy(buffer, salt.length);

    return [buffer.toString('base64'), undefined];
  } catch (error) {
    return [undefined, error];
  }
}

async function verify(password, hash) {
  try {
    const buffer = Buffer.from(hash, 'base64');
    const salt = buffer.slice(0, keyLength);
    const keyA = buffer.slice(keyLength, keyLength * 2);

    const keyB = await pbkdf2(password, salt, iterations, keyLength, digest);

    return [keyA.compare(keyB) === 0, undefined];
  } catch (error) {
    return [undefined, error];
  }
}

module.exports = {
  hash,
  verify,
};
