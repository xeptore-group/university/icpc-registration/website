const _ = require('lodash');
const axios = require('axios');
const { idpay: idpayConfig } = require('../lib/config');
const logger = require('../logger.server');
const persistLogger = require('./logger');


const IdpayErrors = {
  InternalError: 'internal_error',
  Failed: 'failed',
};

class IdpayPayment {
  constructor() {
    const headers = {
      'X-API-KEY': idpayConfig.apiKey,
    };

    if (idpayConfig.useSandbox) {
      headers['X-SANDBOX'] = true;
    }

    const config = {
      baseURL: idpayConfig.requestBaseUrl,
      validateStatus: () => true,
      timeout: 60000,
      headers,
    };

    persistLogger.debug('idpay service is initializing', { config });

    this._client = axios.create(config);

    logger.info('IDPay client successfully created.');
  }

  async request({ orderId, amount, name, phone, mail, description }) {
    persistLogger.debug('idpay.service.request', {
      payload: { orderId, amount, name, phone, mail, description },
    });

    try {
      const response = await this._client.post(
        idpayConfig.paymentRequestUrl,
        {
          order_id: orderId,
          amount,
          name,
          phone,
          mail,
          desc: description,
          callback: idpayConfig.callbackUrl,
        },
      );

      if (response.status === 201) {
        return [{
          id: response.data.id,
          link: response.data.link,
        }, undefined];
      }

      persistLogger.error('idpay.service.payment.request.response.received', {
        payload: {
          orderId,
          amount,
          name,
          phone,
          mail,
          description,
          response: _.omit(response, ['request']),
        },
      });

      return [undefined, {
        status: response.status,
        body: response.data,
      }];
    } catch (error) {
      return [undefined, error];
    }
  }

  async verify({ id, orderId }) {
    persistLogger.debug('idpay.service.verify', {
      payload: { id, orderId },
    });

    try {
      const response = await this._client.post(
        idpayConfig.verifyUrl,
        {
          id,
          order_id: orderId,
        },
      );

      if (response.status === 200) {
        return [response.data, undefined];
      }

      persistLogger.error('idpay.service.payment.verification.request.response.received', {
        payload: { id, orderId, response: _.omit(response, ['request']) },
      });

      return [undefined, {
        status: response.status,
        body: response.data,
      }];
    } catch (error) {
      return [undefined, error];
    }
  }
}

module.exports = {
  IdpayPayment,
  IdpayErrors,
};
