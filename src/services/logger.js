const { createLogger, format, transports } = require('winston');
const config = require('../lib/config');


const loggerConf = config.logging.persistent;

const { colorize, prettyPrint, combine, errors, timestamp, json } = format;

const loggingTransports = [];

if (loggerConf.file) {
  if (loggerConf.error) {
    loggingTransports.push(
      new transports.File({
        filename: loggerConf.errorLogFileName,
        level: 'error',
      }),
    );
  }

  if (loggerConf.warning) {
    loggingTransports.push(
      new transports.File({
        filename: loggerConf.warningLogFileName,
        level: 'warn',
      }),
    );
  }

  if (loggerConf.info) {
    loggingTransports.push(
      new transports.File({
        filename: loggerConf.infoLogFileName,
        level: 'info',
      }),
    );
  }

  if (loggerConf.debug) {
    loggingTransports.push(
      new transports.File({
        filename: loggerConf.debugLogFileName,
        level: 'debug',
      }),
    );
  }
}

const logger = createLogger({
  format: combine(
    timestamp(),
    errors({ stack: true }),
    json(),
  ),
  transports: loggingTransports,
});

if (loggerConf.cli) {
  logger.add(new transports.Console({
    format: combine(
      prettyPrint({
        colorize: true,
        depth: 4,
      }),
      colorize({
        all: true,
        colors: {
          info: 'blue',
          error: 'red',
        },
      }),
    ),
  }));
}

module.exports = logger;
