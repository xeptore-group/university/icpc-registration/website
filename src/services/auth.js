const { verifyToken } = require('./jwt');


async function authenticate(token) {
  if (!token) {
    return [undefined, new Error('token is required.')];
  }

  const [decode, errVerify] = await verifyToken(token);
  return [decode, errVerify];
}

module.exports = {
  authenticate,
};
