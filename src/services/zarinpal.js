const { promisify } = require('util');
const { encode } = require('querystring');
const soap = require('soap');
const join = require('url-join');
const { zarinpal: zarinpalConfig } = require('../lib/config');
const logger = require('../logger.server');
const persistLogger = require('./logger');


const ZarinpalErrors = {
  InternalError: 'internal_error',
  Failed: 'failed',
};

class ZainpalPayment {
  allMethodsAreAvailable() {
    return ['PaymentRequest', 'PaymentVerification']
      .every((fn) =>
        Object.keys(this._client).includes(fn),
      );
  }

  constructor() {
    soap.createClientAsync(zarinpalConfig.soapServerUrl, {
      disableCache: true,
    })
      .then((client) => {
        this._client = client;
        if (!this.allMethodsAreAvailable()) {
          throw new Error('not all zarinpal required methods are available in received soap client');
        }

        this._paymentRequest = promisify(client.PaymentRequest);
        this._verifyPayment = promisify(client.PaymentVerification);
        logger.info('Zarinpal soap client successfully created');
      })
      .catch(error => {
        logger.error(error, 'Unable to create Zarinpal soap client');
      });
  }

  async request({ amount, email, phone, description, transactionId }) {
    persistLogger.debug('zarinpal.service.request', {
      payload: { amount, email, phone, description, transactionId },
    });

    try {
      const payment = {
        MerchantID: zarinpalConfig.merchant,
        Amount: amount,
        Description: description,
        Email: email,
        Mobile: phone,
        CallbackURL: join(
          zarinpalConfig.callbackBaseUrl,
          'verify-payment',
          '?' + encode({ amount, transaction_id: transactionId }),
        ),
      };
      const result = await this._paymentRequest(payment);
      persistLogger.debug('zarinpal.client.request', { result });

      if (parseInt(result.Status) === 100) {
        const ok = true;
        const url = join(zarinpalConfig.startPaymentUrl, result.Authority);
        const out = { ok, authority: result.Authority, url, error: undefined };
        return [out, undefined];
      } else {
        const ok = false;
        const out = { ok, authority: '', url: '', error: result.Status };
        return [out, undefined];
      }
    } catch (error) {
      return [undefined, error];
    }
  }

  async verify({ status, amount, authority }) {
    persistLogger.debug('zarinpal.service.verify', {
      payload: { status, amount, authority },
    });

    if (status !== 'OK') {
      return [undefined, ZarinpalErrors.Failed];
    }

    try {
      const verification = {
        MerchantID: zarinpalConfig.merchant,
        Amount: amount,
        Authority: authority,
      };
      const result = await this._verifyPayment(verification);
      persistLogger.debug('zarinpal.client.verify', { result });

      if (parseInt(result.Status) === 100) {
        const ok = true;
        const refId = result.RefID;
        return [{ ok, refId, error: undefined }, undefined];
      } else {
        const ok = false;
        return [{ ok, refId: '', error: result.Status }, undefined];
      }
    } catch (error) {
      return [undefined, error];
    }
  }
}

module.exports = {
  ZainpalPayment,
  ZarinpalErrors,
};
