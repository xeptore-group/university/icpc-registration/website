const config = require('./config');
const mongoose = require('mongoose');
const logger = require('../logger.server');


const dbConf = config.database;
async function connect() {
  const opts = {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true,
  };

  if (dbConf.useAuth) {
    opts.pass = dbConf.password;
    opts.user = dbConf.user;
  }

  const connectionUri = `${dbConf.host}:${dbConf.port}/${dbConf.name}`;

  try {
    await mongoose.connect(`mongodb://${connectionUri}`, opts);

    logger.info('Database successfully connected');
  } catch (error) {
    logger.error('Error connecting database', error);
  }
}

async function disconnect() {
  return mongoose.connection;
}

module.exports = {
  connect,
  connection: mongoose.connection,
  disconnect,
};
