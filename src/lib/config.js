const config = require('config');


module.exports = {
  server: {
    port: parseInt(config.get('server.port')),
    trustProxy: Boolean(config.get('server.trust_proxy')),
    domain: config.get('server.domain'),
  },
  logging: {
    simple: {
      enabled: Boolean(config.get('logging.simple.enabled')),
      verbose: Boolean(config.get('logging.simple.verbose')),
      logRequestBody: Boolean(config.get('logging.simple.log_request_body')),
      logResponseBody: Boolean(config.get('logging.simple.log_response_body')),
      file: Boolean(config.get('logging.simple.file')),
      logFileName: config.get('logging.simple.log_file_name'),
    },
    persistent: {
      cli: Boolean(config.get('logging.persistent.cli')),
      file: Boolean(config.get('logging.persistent.file')),
      error: Boolean(config.get('logging.persistent.error')),
      info: Boolean(config.get('logging.persistent.info')),
      warning: Boolean(config.get('logging.persistent.warning')),
      debug: Boolean(config.get('logging.persistent.debug')),
      errorLogFileName: config.get('logging.persistent.error_log_file_name'),
      infoLogFileName: config.get('logging.persistent.info_log_file_name'),
      warningLogFileName: config.get('logging.persistent.warning_log_file_name'),
      debugLogFileName: config.get('logging.persistent.debug_log_file_name'),
    },
  },
  database: {
    host: config.get('database.host'),
    port: parseInt(config.get('database.port')),
    name: config.get('database.name'),
    user: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    useAuth: Boolean(config.get('database.use_auth')),
  },
  zarinpal: {
    merchant: process.env.ZARINPAL_MERCHANT,
    soapServerUrl: config.get('zarinpal.soap_server_url'),
    callbackBaseUrl: config.get('zarinpal.callback_base_url'),
    startPaymentUrl: config.get('zarinpal.start_payment_url'),
  },
  idpay: {
    apiKey: process.env.IDPAY_API_KEY,
    paymentRequestUrl: config.get('idpay.payment_request_path'),
    verifyUrl: config.get('idpay.verify_request_path'),
    callbackUrl: config.get('idpay.callback_base_url'),
    requestBaseUrl: config.get('idpay.request_base_url'),
    useSandbox: Boolean(config.get('idpay.use_sandbox')),
  },
  payment: {
    enabled: Boolean(config.get('payment.enabled')),
    description: config.get('payment.description'),
  },
};
