const paymentModel = require('../models/payment');


async function persist({
  teamId,
  orderId,
  iid,
  link,
  amount,
  name,
  phone,
  description,
}) {
  const model = new paymentModel({
    teamId,
    orderId,
    iid,
    link,
    amount,
    name,
    phone,
    description,
    trackId: '',
    cardNo: '',
    hashedCardNo: '',
    paidAt: '',
    verifiedAt: '',
    verified: false,
  });

  try {
    const saved = await model.save();

    return [saved, undefined];
  } catch (error) {
    return [undefined, error];
  }
}

async function update({
  id,
  trackId,
  cardNo,
  hashedCardNo,
  paidAt,
  verified,
  verifiedAt,
}) {
  try {
    await paymentModel.updateOne(
      { _id: id },
      {
        trackId,
        cardNo,
        hashedCardNo,
        paidAt,
        verifiedAt,
        verified,
      },
    );

    return undefined;
  } catch (error) {
    return error;
  }
}

async function getByOrderId(orderId) {
  try {
    const payment = await paymentModel.findOne({ orderId }).exec();

    return [payment, undefined];
  } catch (error) {
    return [undefined, error];
  }
}

async function verifiedPaymentExists({ trackId, iid }) {
  try {
    const payment = await paymentModel.findOne().or([
      { trackId },
      { iid },
    ])
      .and([
        { verified: true },
      ])
      .select(['_id'])
      .exec();

    return [payment !== null, undefined];
  } catch (error) {
    return [undefined, error];
  }
}

module.exports = {
  persist,
  update,
  getByOrderId,
  verifiedPaymentExists,
};
