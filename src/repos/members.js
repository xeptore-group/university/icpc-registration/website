const { memberModel, teamModel } = require('../models');


async function memberExists({ nationalId, studentId, mobile }) {
  try {
    const member = await memberModel.findOne({}).or([
      { nationalId },
      { studentId },
      { mobile },
    ]).select(['_id']).exec();

    return [member !== null, undefined];
  } catch (error) {
    return [undefined, error];
  }
}

async function registerMemeber(member) {
  const model = new memberModel({
    firstName: member.firstName,
    lastName: member.lastName,
    isHead: member.isHead,
    mobile: member.mobile,
    studentId: member.studentId,
    nationalId: member.nationalId,
    teamId: member.teamId,
    password: member.password,
    joinedAt: member.joinedAt,
  });

  try {
    const saved = await model.save();

    return [saved, undefined];
  } catch (error) {
    return [undefined, error];
  }
}

async function assignMemberToTeam(memberId, teamId) {
  try {
    await memberModel.updateOne({ _id: memberId }, { teamId }).exec();

    return undefined;
  } catch (error) {
    return error;
  }
}

async function deleteMember(memberId) {
  try {
    await memberModel.deleteOne({ _id: memberId }).exec();

    return undefined;
  } catch (error) {
    return error;
  }
}

async function findHeadMemberWithPassword(studentId) {
  try {
    const member = await memberModel
      .findOne({ studentId, isHead: true })
      .select(['_id', 'password'])
      .exec();

    return [member, undefined];
  } catch (error) {
    return [undefined, error];
  }
}

async function getMemberTeam(memberId) {
  try {
    const team = await teamModel
      .findOne({ registrarId: memberId })
      .select(['paymentAmount', 'paid', 'name'])
      .exec();

    return [team, undefined];
  } catch (error) {
    return [undefined, error];
  }
}

async function getMemberById(memberId) {
  try {
    const member = await memberModel
      .findById( memberId )
      .exec();

    return [member, undefined];
  } catch (error) {
    return [undefined, error];
  }
}

module.exports = {
  memberExists,
  registerMemeber,
  assignMemberToTeam,
  findHeadMemberWithPassword,
  getMemberTeam,
  deleteMember,
  getMemberById,
};
