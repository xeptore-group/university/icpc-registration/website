const { teamModel, memberModel } = require('../models');


async function isTeamNameUnique(teamName) {
  try {
    const team = await teamModel.findOne({ name: teamName }).select(['_id']).exec();

    return [team !== null, undefined];
  } catch (error) {
    return [undefined, error];
  }
}

async function registerTeam(team) {
  const model = new teamModel({
    name: team.name,
    registeredAt: team.registeredAt,
    registrarId: team.registrarId,
    paymentAmount: team.paymentAmount,
    paid: team.paid,
    memberIds: team.memberIds,
  });

  try {
    const saved = await model.save();

    return [saved, undefined];
  } catch (error) {
    return [undefined, error];
  }
}

async function teamExists(teamId) {
  try {
    const team = await teamModel
      .findOne({ _id: teamId })
      .select(['_id'])
      .exec();

    return [team !== null, undefined];
  } catch (error) {
    return [undefined, error];
  }
}

async function getTeamById(teamId) {
  try {
    const team = await teamModel
      .findById(teamId)
      .select([
        'name',
        'memberIds',
        'paymentAmount',
        'paid',
      ])
      .exec();

    return [team, undefined];
  } catch (error) {
    return [undefined, error];
  }
}

async function getTeamMembers(teamId) {
  try {
    const members = await memberModel
      .find({ teamId })
      .select([
        'firstName',
        'lastName',
        'mobile',
        'isHead',
        'studentId',
        'nationalId',
      ])
      .exec();

    return [members, undefined];
  } catch (error) {
    return [undefined, error];
  }
}

async function deleteTeamMember(teamId, memberId) {
  try {
    await teamModel
      .updateOne({ _id: teamId }, { $pull: { memberIds: memberId } })
      .exec();

    return undefined;
  } catch (error) {
    return error;
  }
}

async function getTeamMembersCount(teamId) {
  try {
    const team = await teamModel
      .findOne({ _id: teamId })
      .select(['memberIds'])
      .exec();

    return [team.memberIds.length, undefined];
  } catch (error) {
    return [undefined, error];
  }
}

async function addTeamMember(teamId, memberId) {
  try {
    await teamModel
      .updateOne({ _id: teamId }, { $push: { memberIds: memberId } })
      .exec();

    return undefined;
  } catch (error) {
    return error;
  }
}

async function updateTeamPaymentAmount(teamId, newPaymentAmount) {
  try {
    await teamModel
      .updateOne({ _id: teamId }, { paymentAmount: newPaymentAmount })
      .exec();

    return undefined;
  } catch (error) {
    return error;
  }
}

async function setTeamPaid(teamId) {
  try {
    await teamModel
      .findByIdAndUpdate(teamId, { paid: true })
      .exec();

    return undefined;
  } catch (error) {
    return error;
  }
}

module.exports = {
  isTeamNameUnique,
  registerTeam,
  getTeamMembers,
  teamExists,
  addTeamMember,
  getTeamMembersCount,
  getTeamById,
  updateTeamPaymentAmount,
  deleteTeamMember,
  setTeamPaid,
};
