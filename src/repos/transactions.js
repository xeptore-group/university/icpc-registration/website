const transactionModel = require('../models/transaction');


async function create({
  description = '',
  paymentAmount,
  mobile,
  teamId,
  authority,
}) {
  const transaction = new transactionModel({
    description,
    createdAt: new Date().toISOString(),
    verifiedAt: 'na',
    verificationStatus: 'na',
    paymentAmount,
    mobile,
    teamId,
    authority,
    refId: 'na',
  });

  try {
    const createdTransaction = await transaction.save();

    return [createdTransaction, undefined];
  } catch (error) {
    return [undefined, error];
  }
}

async function updateAuthority({
  transactionId,
  authority,
}) {
  try {
    await transactionModel.updateOne({ _id: transactionId }, { authority });

    return undefined;
  } catch (error) {
    return error;
  }
}


async function verify({
  transactionId,
  verifiedAt,
  verificationStatus,
  refId,
}) {
  try {
    await transactionModel.updateOne({ _id: transactionId }, {
      verifiedAt,
      verificationStatus,
      refId,
    });

    return undefined;
  } catch (error) {
    return error;
  }
}

async function getById(transactionId) {
  try {
    const transaction = await transactionModel.findById(transactionId).exec();

    return [transaction, undefined];
  } catch (error) {
    return [undefined, error];
  }
}

module.exports = {
  create,
  updateAuthority,
  verify,
  getById,
};
